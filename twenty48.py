#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Project:    2048
File:       twenty48.py
Author:     Korvin F. Ezüst

Created:    2017-11-29
IDE:        PyCharm Community Edition

Description:
            A 2048 game running in the command line
"""

import random
import sys
import tty
import termios

__author__ = "Korvin F. Ezüst"
__copyright__ = "Copyright (c) 2017., Korvin F. Ezüst"
__license__ = "Apache 2.0"
__version__ = "2.0"
__email__ = "dev@korvin.eu"
__status__ = "Development"


# TODO: Make keypress input platform-independent
# TODO: Test on Windows


class Board:
    def __init__(self, size):
        self.size = size
        self.array = self.generate_board()

    def get_position_on_board(self, i):
        """
        A random number will be generated for the board.
        Get the coordinates of that number on the board.

        :param i: a position to be picked on the board
        :type i: int
        :return: coordinates on the board
        :rtype: tuple
        """

        return (i // self.size), (i % self.size)

    def generate_board(self):
        """
        Generates the game board in a 2D, x by x list.
        Populates the board with 0s, and two 2s at random positions.

        :return: the board
        :rtype: list
        """

        # make board with all 0s
        array = [[0 for _ in range(self.size)] for _ in range(self.size)]
        # get two different random ints
        i = j = random.randint(0, self.size ** 2 - 1)
        while i == j:
            j = random.randint(0, self.size ** 2 - 1)
        # place them on the board
        i = self.get_position_on_board(i)
        j = self.get_position_on_board(j)
        array[i[0]][i[1]] = 2
        array[j[0]][j[1]] = 2

        return array

    def flatten(self):
        """
        "Flatten" the board into a 1D list.

        :return: "flattened" board
        :rtype: list
        """

        return [item for sub in self.array for item in sub]

    def move_left(self):
        """
        Left move.
        In each row, get rid of intermittent zeroes
        and put them back at the end of the row.
        In each row, merge same numbers next to each other
        then put intermittent zeroes at the end of each row.
        """

        def _push_zeroes_right(_array):
            # Get number of zeroes
            count = _array.count(0)
            # Delete all zeroes
            while 0 in _array:
                _array.remove(0)
            # Append counted zeroes to the end
            for _ in range(count):
                _array.append(0)

        # Rearrange board by moving zeroes from in-between numbers
        for i in range(self.size):
            _push_zeroes_right(self.array[i])

        # Row indices
        for i in range(self.size):
            # Column indices
            for j in range(self.size):
                # Make sure column index stays in range
                if j < self.size - 1:
                    # If at row i cell j and j+1 are equals
                    if self.array[i][j] == self.array[i][j + 1]:
                        # double cell j
                        self.array[i][j] *= 2
                        # rewrite cell j+1 with 0
                        self.array[i][j + 1] = 0
            # Move zeroes to the right
            # after every merge is complete in row i
            _push_zeroes_right(self.array[i])

    def move_right(self):
        """
        Reverse the board and treat move as left move and reverse again.
        """

        def _reverse():
            tmp = []
            rev_array = []
            for row in self.array:
                for i in row[::-1]:
                    tmp.append(i)
                rev_array.append(tmp)
                tmp = []
            return rev_array

        self.array = _reverse()
        self.move_left()
        self.array = _reverse()

    def rotate_90(self):
        """
        Rotate the board by 90°.

        :return: rotated board
        :rtype: list
        """

        tmp = []
        rotated = []

        for i in range(len(self.array)):
            # Get last item from every row
            # and append it to a temporary list
            for row in self.array[::-1]:
                tmp.append(row[i])
            # Append temporary list to rotated list
            rotated.append(tmp)
            tmp = []

        return rotated

    def move_down(self):
        """
        Rotate board by 90°, treat move as left move,
        and rotate board back.
        """

        self.array = self.rotate_90()
        self.move_left()
        for _ in range(3):
            self.array = self.rotate_90()

    def move_up(self):
        """
        Rotate board by 270°, treat move as left move,
        and rotate board back.
        """

        for _ in range(3):
            self.array = self.rotate_90()
        self.move_left()
        self.array = self.rotate_90()

    @staticmethod
    def get_first_zero(start, list_):
        """
        Find the first zero in a list counted from a start position.
        If the search reaches the end of the list,
        start over from the beginning til the start.
        If there's no zero, return None.

        :param start: starting position
        :type start: int
        :param list_: flattened array
        :type list_: list
        :return: position of zero or None
        :rtype: int|None
        """

        for i, item in enumerate(list_[start:]):
            if item == 0:
                return i + start
        for i, item in enumerate(list_[:start]):
            if item == 0:
                return i
        return None

    def end_game_check(self):
        """
        Check if there are two same numbers next to or above/below
        each other. Returns False if there are, True otherwise.

        :return: game end or not
        :rtype: bool
        """

        # Rotate list
        rotated = self.rotate_90()
        # Row indices
        for i in range(self.size):
            # Column indices
            for j in range(self.size):
                # Make sure j stays in range
                if j < self.size - 1:
                    # Return False if there's two same numbers
                    # next to or above/below each other
                    if self.array[i][j] == self.array[i][j + 1]:
                        return False
                    if rotated[i][j] == rotated[i][j + 1]:
                        return False
        # Return True if the loops end
        return True

    def new_two(self):
        """
        Place a 2 on a random 0.
        If there's no 0 left, check if there's valid move left.
        Return 1 if there's no valid move left

        :return: 1 if nothing to do
        :rtype: int
        """
        fb = self.flatten()
        # Get a random position on the board
        # and get the first 0's position from that random position
        r = random.randrange(0, self.size ** 2)
        p = self.get_first_zero(r, fb)
        # When a new 2 cannot be placed
        # exit game if no valid moves left
        # otherwise return board unchanged
        if p is None:
            if self.end_game_check():
                return 1
        # Place new 2 on the board
        else:
            bp = self.get_position_on_board(p)
            self.array[bp[0]][bp[1]] = 2

    def __str__(self):
        """
        Modify the print statement to print the board
        in a presentable way.
        :return: string of the board to be printed
        :rtype: str
        """

        # Find the biggest number
        # and set the padding to its length as a string +1
        ret = ""
        pad = len(str(max(self.flatten()))) + 1
        for row in self.array:
            for col in row:
                if col == 0:
                    # Set padding and align based on
                    # the biggest number on the board
                    ret += ("{:>{}} ".format("_", pad))
                else:
                    ret += ("{:>{}} ".format(col, pad))
            ret += "\n\n"
        return ret


def clear():
    """
    Clear the command line screen.
    TODO: Make it platform-independent
    """

    print("\033[H\033[J")


def get_board_size():
    """
    Keep asking for player input until it's an integer
    between min_ and max_.

    :return: board size
    :rtype: int
    """

    min_ = 4
    max_ = 8
    clear()
    while True:
        try:
            size = int(input(f"Size of board ({min_}-{max_}): "))
            if min_ <= size <= max_:
                return size
            else:
                raise ValueError
        except ValueError:
            print(
                f"Invalid input. The size has to be\n"
                f"an integer between {min_} and {max_}")


def get_key():
    """
    Read a single keypress from stdin and return the resulting character.
    Nothing is echoed to the console. This call will block if a keypress
    is not already available, but will not wait for Enter to be pressed.

    If the pressed key was a modifier key, nothing will be detected; if
    it were a special function key, it may return the first character of
    of an escape sequence, leaving additional characters in the buffer.

    Source: https://stackoverflow.com/a/32671356/7065519

    :return: a single character
    :rtype: str
    """
    fd = sys.stdin.fileno()
    old_settings = termios.tcgetattr(fd)
    try:
        tty.setraw(fd)
        ch = sys.stdin.read(1)
    finally:
        termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
    return ch


if __name__ == "__main__":
    board = False
    input_ = ""

    # Valid inputs. Must be upper case
    up = "W"
    down = "S"
    left = "A"
    right = "D"
    nw = "N"
    ex = "X"
    while input_ != ex:
        # Start new game if board doesn't exist
        if not board:
            x = get_board_size()
            board = Board(x)

        clear()
        print(f"Press:  {up} : Up   | {down} : Down\n"
              f"        {left} : Left | {right} : Right\n"
              f"Press {ex} to leave the game\n"
              f"Press {nw} to start a new game\n\n")
        print(board)

        # Keep asking player for input until it's valid
        input_ = ""
        while input_ not in (up, down, left, right, nw, ex):
            input_ = get_key().upper()

        # Make move or set up board for new game
        if input_ == up:
            board.move_up()
        elif input_ == down:
            board.move_down()
        elif input_ == left:
            board.move_left()
        elif input_ == right:
            board.move_right()
        elif input_ == nw:
            board = False
            clear()

        # If player asked for new move, call function to add a new 2
        # Check if the return is 1, end the game if it is
        if input_ in (up, down, left, right):
            stat = board.new_two()
            if stat == 1:
                print("Game over.")
                sys.exit()
